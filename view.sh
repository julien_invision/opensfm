#!/bin/bash
set -e

DIR="${1:-$(pwd)}"
RECONSTRUCTION="${DIR}/reconstruction.json"
if [ -r "${RECONSTRUCTION}" ] ; then
    xdg-open "http://localhost:8088/viewer/#file=data/reconstruction.json&image=data/images"

    docker run --rm -it -p 8088:8088 -v "${DIR}":/source/OpenSfM/data registry.gitlab.com/julien_invision/opensfm:2022-02-10 python3 -m http.server 8088 
else
    echo "Can't find ${RECONSTRUCTION}."
    echo "Usage: $0 <path to reconstruction folder>"
    echo "If folder is ommitted, current directory is used."
    exit 1
fi

