#!/bin/bash
set -e
IMAGE="registry.gitlab.com/julien_invision/opensfm:$(git show "--format=format:%ci" -s HEAD | sed 's/ .*$//')"
echo "imagename:" $IMAGE
docker run -p 8000:8000 \
    -v $(pwd)/data:/source/OpenSfM/data \
    -v /tmp:/tmp \
    -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro --user $(id -u):$(id -g) \
    -it --rm $IMAGE ${*:-bash}
