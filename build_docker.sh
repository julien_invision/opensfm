#!/bin/bash
set -e

IMAGE=${IMAGE:-"registry.gitlab.com/julien_invision/opensfm:$(git show "--format=format:%ci" -s HEAD | sed 's/ .*$//')"}

docker build -f Dockerfile.ceres2 -t $IMAGE .
docker push $IMAGE
