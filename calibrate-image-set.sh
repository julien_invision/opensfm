#!/bin/bash

set -e

if [ $# -lt 1 ]; then
    echo 'usage: $0 <input_folder> [needs to contain an images directory]'
    exit 1
fi

input_folder="$1"

if [ "${input_folder:0:1}" == "-" ]; then
    echo 'usage: $0 <input_folder> [needs to contain an images directory]'
    exit 1
fi

if [ ! -d "${input_folder}/images" ]; then
    echo 'usage: $0 <input_folder> [needs to contain an images directory]'
    exit 1
fi

echo ${input_folder}

docker container run -it --rm \
    -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro --user $(id -u):$(id -g) \
    -v "${input_folder}"/:/external/in \
    -v "${output_folder}":/external/out \
    calibration_docker
